import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import numpy as np

df = pd.read_csv("clustered.csv")

geo_data = df.to_numpy()[:, 1:]
geo_data[:, 0] = geo_data[:, 0].astype(int)

fig = go.Figure(go.Scattermapbox(mode="markers"))

for i in range(30):
    _filter = np.where(geo_data[:, 0] == i)[0]
    cluster = geo_data[_filter]

    fig.add_trace(go.Scattermapbox(lat=cluster[:, 1], lon=cluster[:, 2]))

fig.update_layout(
    mapbox={
        "style": "open-street-map"
    },
    margin={"l": 0, "r": 0, "t": 0, "b": 0},
)
fig.show()
