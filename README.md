

    * Estudar acidentes primeiro por estado. Qual estado possui mais acidentes?
    * qual periodo do dia há mais acidentes em cada estado?
    * Quais tipos de acidentes são mais comuns nos periodos do dia, em cada estado?
        (Por exemplo supomos que o RS tem muito acidente no periodo da noite, e a maior causa é embriagues.
         dessa forma indentifica-se um padrão de que aquele estado pode ter menor fiscalização nessa area. Isso 
         na vdd é uma pergunta a se fazer e tentar responder: Pq tal estado tem maior indice nesse quesito?)
    * Tentar normalizar os dados de numero de acidentes por estado, pra se ter uma comparação mais justa.
    * Agrupar numero de acidentes por BR, e normalizar a br pelo seu comprimento, dps compara a quantidade de acidentes nessas brs,
      e também verificar o periodo do dia que isso ocorreu.
    * Agrupar os acidentes por cluster, e tirar as mesmas informções de numero de acidentes, periodo do dia, tipo de acidente. Ver se
      algum cluster se destaca se forma especifica, podendo indicar uma região anomala, e assim tentar responder a pergunta: Pq isso acontece
      nesse cluster (talvez agrupar por lat, long e tipo de acidente)?
    
    * Analisando por Km de estrada, ver trechos que mais levam acidentes com morte. Plotar isso no mapa pra ver essas regiões que mais se destacam, tentar
      responder a pergunta: O que tem nessas regições? Qual a faixa de idade/ genero que mais leva acidentes com mortes? Analisar condição meterologica nesses acidentes.
      O tipo de clima afeta muito acidentes fatais?
    * Analisar o tipo de acidente de acordo com genero/idade. Exemplo: De X acidentes, a maioria das causas principais são causados por homens entre 20-30 anos.
      Possibilidade de verificar se existem regiões onde muda essa estatistica. Exemplo: na região y a predominancia muda, e a maioria dos acidentes são causados 
      por mulheres de 50-60 anos. Porque isso ocorre?
    * Verificar o tipo de carro/ano de fabricação que mais causa acidentes (considerando o dataset total). Existe alguma relação? Existe um padrão/marca de carro especifica
      que causa mais acidentes? e acidentes fatais?
    * Verificar correlação entre tipo do envolvido (passageiro, condutor, pedestre, etc) e acidentes. Tentar responder a seguinte pergunta: Carros com mais gente dentro do carro,
      tem maior probabiidade de sofrer acidente? Ou será que uma pessoa dirigindo sozinha é mais provavel de sofrer um acidente do que com carro cheio?
